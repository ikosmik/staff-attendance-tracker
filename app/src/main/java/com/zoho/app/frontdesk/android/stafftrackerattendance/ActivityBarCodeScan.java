package com.zoho.app.frontdesk.android.stafftrackerattendance;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.vision.CameraSource;
import com.google.zxing.ResultPoint;
import com.google.zxing.integration.android.IntentIntegrator;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.camera.CameraSettings;
import com.zoho.app.frontdesk.android.stafftrackerattendance.Constants.MainConstants;
import com.zoho.app.frontdesk.android.stafftrackerattendance.backgroundProcess.service.BackgroundService;
import com.zoho.app.frontdesk.android.stafftrackerattendance.backgroundProcess.storeEmployees;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.DataBaseRepository;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.EmployeeEntity;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.ShiftsEntity;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.VisitorEntity;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ActivityBarCodeScan extends Activity {

    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This activity is used to Scan employee login code from barCode, If employee found it will allow to punch In/Out.
   */
    private Handler handler = new Handler();
    private DecoratedBarcodeView barcodeReader;
    private TextView text_view_scanner_title,text_status_glow;
    private IntentIntegrator qrScan;
    Context context;
    private CaptureManager capture;
    private DataBaseRepository dataBaseRepository;
    private String Employee_id;
    private String Employee_name;
    private String Employee_login_code;
    private String Employee_shift_id;
    final String json_time_pattern = "dd-MMM-yyyy";
    private DataBaseRepository databaseBaseRepository;
    private List<VisitorEntity> incomplete_attendance;
    private List<VisitorEntity> completed_attendance;
    private List<VisitorEntity> employee_list;
    private storeEmployees store_employees;
    // here creating required variable
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            // Remove the title bar.
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            // hide the status bar.
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            // here setting the xml layout
            setContentView(R.layout.layout_scanner);
            this.context = this;
            assign_objects(savedInstanceState);
        }
        catch (Exception e){
            Log.e("ERROR", "*** Scanner activity error : " + e.getMessage());
        }
    }
    //
    private void assign_objects(Bundle savedInstanceState){
        // here setting all elements
        text_view_scanner_title = findViewById(R.id.text_view_scanner_title);
        text_status_glow = findViewById(R.id.text_status_glow);
        barcodeReader = (DecoratedBarcodeView)findViewById(R.id.barcode_scanner_view);
        qrScan = new IntentIntegrator(this);
        show_color_signal(0);
        StartScanning(savedInstanceState);
    }
    //
    public void StartScanning(Bundle savedInstanceState){
        //barcodeReader.initializeFromIntent(qrScan.createScanIntent());
        try {
            qrScan.setCameraId(CameraSource.CAMERA_FACING_BACK);
            //
            CameraSettings cameraSettings = new CameraSettings();
            cameraSettings.setRequestedCameraId(1);
            qrScan.setBeepEnabled(true);
            qrScan.setBarcodeImageEnabled(true);
            qrScan.setOrientationLocked(false);
            barcodeReader.setFocusable(true);
            capture = new CaptureManager(this, barcodeReader);
            capture.initializeFromIntent(qrScan.createScanIntent(), savedInstanceState);
            barcodeReader.decodeSingle(get_results);
        }
        catch (Exception e)
        {
            Log.e("ERROR","qrScan ********* "+e.getMessage());
        }
    }
    //
    public void show_color_signal(int code){
        //
        switch (code){
            case 0:
                // color grey
                text_status_glow.setBackgroundColor(Color.parseColor(MainConstants.color_grey));
                break;
            case 1:
                // color green
                text_status_glow.setBackgroundColor(Color.parseColor(MainConstants.color_green));
                break;
            case 2:
                // color red
                text_status_glow.setBackgroundColor(Color.parseColor(MainConstants.color_red));
                break;
            default:
                // color grey
                text_status_glow.setBackgroundColor(Color.parseColor(MainConstants.color_blue));
                break;
        }
        //
        if(handler != null){
            handler = null;
            handler = new Handler();
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Calling scanner activity
                text_status_glow.setBackgroundColor(Color.parseColor(MainConstants.color_grey));
            }

        }, 3000);
        //
    }
    //
    BarcodeCallback get_results = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            try {
                Employee_login_code = "";
                Employee_id = "";
                Employee_name = "";
                Employee_shift_id = "";
                //here setting the time delay for action handler
                Log.e("Scanned", "******* result.getContents() : " +result.getText());
                if(result.getText() != null && !result.getText().isEmpty()){
                    //here storing scanned value to variable if it is not empty
                    String bar_code_value = result.getText();
                    String message = MainConstants.kConstantEmpty;
                    Log.e("Scanned", "******* Scanner bar_code_value : " + bar_code_value);
                    Log.e("Scanned", "******* Employee bar_code_value : " + Employee_login_code);
                    if(bar_code_value != null && !bar_code_value.isEmpty()){
                        //
                        int is_employee_found = getEmployeeFromLoginCode(bar_code_value);
                        //
                        if(is_employee_found > 0)
                        {
                            // TODO Show green light and add attendance
                            show_color_signal(1);
                            add_attendance();
                        }
                        else
                            {
                                show_color_signal(2);
                                // TODO Show red light and show error
                        }
                        //here checking if scanned data is matches with our requirement using regex
                        }
                    else{
                        // TODO Show red light and show error
                        show_color_signal(2);
                        //here setting error messages based on conditions
                        message = "Unable to Scan the BarCode, Please try again";
                        //here showing error messages on UI
                    }
                }
            }
            catch (Exception e){
                Log.e("ERROR", "******* Scanning Error : " + e.getMessage());
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {

        }
    };
    //
    @Override
    public void onResume() {
        super.onResume();
        if(barcodeReader != null){
            barcodeReader.resume();
        }
        if(capture != null){

            capture.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(barcodeReader != null){
            barcodeReader.pause();
        }
        if(capture != null){
            capture.onPause();
        }
    }
    //
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(capture != null) {
            capture.onSaveInstanceState(outState);
        }
    }
    //
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeReader.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }
    //

    private int getEmployeeFromLoginCode(String code){
        int size = 0;
        try {
           if(code != null)
           {
               dataBaseRepository = new DataBaseRepository(context);
               List<EmployeeEntity> employee_list = dataBaseRepository.fetchEmployeesByLoginCode(code);
               size = employee_list.size();
               //
               if (employee_list.size() > 0) {
                   //
                   for (int i = 0; i < employee_list.size(); i++) {
                       //
                       if (employee_list.get(i).getEmployee_id() != null
                               && employee_list.get(i).getEmployee_name() != null && !employee_list.get(i).getEmployee_name().isEmpty()) {
                           //
                           Employee_id = employee_list.get(i).getEmployee_id();
                           Employee_name = employee_list.get(i).getEmployee_name();
                           Employee_login_code = employee_list.get(i).getLogin_code();
                       }
                   }
               }
           }
        }
        catch (Exception e){
            Log.e("Error ", "fetching Employees *********** " + e.getMessage());
        }

        return size;
    }
    //
    public String get_shift_id()
    {
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH");
            String formattedDate = dateFormat.format(new Date());
            int hour = Integer.parseInt(formattedDate);
            List<ShiftsEntity> shiftsEntities;
            //
            shiftsEntities = databaseBaseRepository.fetchWorkShift(1);
            for (int i = 0; i < shiftsEntities.size(); i++) {
                //
                String start_time = shiftsEntities.get(i).getStart_time();
                String end_time = shiftsEntities.get(i).getEnd_time();
                if(hour >= Integer.parseInt(start_time.substring(0,2)) && hour < Integer.parseInt(end_time.substring(0,2))){
                    Employee_shift_id = shiftsEntities.get(i).getShift_id();
                    break;
                }
                else{
                    //
                    Employee_shift_id = "";
                }

            }
        }
        catch (Exception e)
        {
            // TODO Log
            Employee_shift_id = "";
        }
        return Employee_shift_id;
    }
    //
    private void add_attendance()
    {
        try {
            String employee_id = Employee_id;
            String employee_name = Employee_name;
            int status = 0;
            String current_time = get_current_time();
            String current_date = get_current_Date();
            boolean is_found = false;
            //
            if(databaseBaseRepository != null)
            {
                String shift_id = get_shift_id();
                //
                is_found = databaseBaseRepository.is_attendance_found(employee_id,current_date);
                completed_attendance = databaseBaseRepository.fetchCompleteAttendance(employee_id,current_date);
                incomplete_attendance = databaseBaseRepository.fetchInCompleteAttendance(employee_id,current_date);
                //
                if(is_found){
                    if (incomplete_attendance.size() > 0) {
                        status = MainConstants.status_log_out;
                        // TODO punch out
                        databaseBaseRepository.update_attendance_record(incomplete_attendance.get(0).getId(), current_time,status);

                        if(checkConnectivity(context)){
                            store_employees = new storeEmployees(context);
                        }
                        show_color_signal(1);
                        Log.e("ATDN* ", " *** Log-out *** ");
                        Log.e("ATDN* ", " ****************************** ");
                    }
                    else if (completed_attendance.size() > 0) {
                        show_color_signal(1);
                        //Already Punched out Today
                        Log.e("ATDN* ", " *** Already Log-out *** ");
                    }
                }
                else
                {
                    status =  MainConstants.status_log_in;;
                    // TODO punch in
                    databaseBaseRepository.insert_attendance_record(employee_id,employee_name,current_time,"",
                            "",1,status,current_date,shift_id);
                    if(checkConnectivity(context)){
                        store_employees = new storeEmployees(context);
                    }
                    show_color_signal(1);
                    // Punched in
                    Log.e("ATDN* ", " *** Punch IN *** ");
                }
            }
            //
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            Log.e("Error ", "Adding attendance error *********** " + e.getMessage());
            show_color_signal(2);
        }
    }
    //
    public String get_current_time()
    {
        //here creating a calendar object
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat(MainConstants.app_date_time_pattern);
            String formattedDate = dateFormat.format(new Date());
            return formattedDate;
        }
        catch (Exception e)
        {
            return "";
        }
    }
    //
    public String get_current_Date()
    {
        //here creating a calendar object
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat(json_time_pattern);
            String formattedDate = dateFormat.format(new Date());
            return formattedDate;
        }
        catch (Exception e)
        {
            return "";
        }
    }
   //this function is used to check if network is available
    @NonNull
    public boolean checkConnectivity(Context context){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network network = connMgr.getActiveNetwork();
            if (network == null){
                return false;
            }
            NetworkCapabilities capabilities = connMgr.getNetworkCapabilities(network);
            if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                return true;
            }
            else if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                return true;
            }
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if(connectivityManager.getActiveNetworkInfo() != null){
                return connectivityManager.getActiveNetworkInfo().isConnected();
            }
            else{
                return false;
            }
        }
        return false;
    }
    //

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler = null;
    }
}
