package com.zoho.app.frontdesk.android.stafftrackerattendance.Constants;

public class MainConstants {

    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This class is used to do hold constant values.
     * */
    public static final String json_time_pattern = "dd-MMM-yyyy";
   public static final int BRIGHTNESS_DIM_TIMEOUT = 120000;
    public static final String kConstantEmpty = "";
    public static final String app_date_time_pattern = "dd-MMM-yyyy HH:mm:ss";
    public static final String kConstantNewLine = "\n";
    public static final String kConstantGetMethod = "GET";
    public static final String kConstantPostMethod = "POST";
    public static final String user_Agent = "Mozilla/5.0";
    public static final String kConstantUserAgent = "User-Agent";
    public static final String kConstantAcceptLanguage = "Accept-Language";
    public static final String acceptLanguage = "en-US,en;q=0.5";

    public static final String app_authtoken = "8e98c8f9a9a8dbcd842ad9dca84ebe57";
    public static final String app_name = "staff-time-tracker";
    //
    public static final int status_log_in = 0;
    public static final int status_log_out = 1;
    public static final int status_login_sent = 2;
    public static final int status_logout_sent = 3;
    //
    public static final String php_get_url = "";
    public static final String php_get_url_shifts = "http://awesomesimple.96.lt/staff_time_tracker/get_st_shifts.php";
    public static final String php_post_url = "";
   public static final String color_grey = "#4C4C4C";
   public static final String color_green = "#6BEC00";
   public static final String color_red = "#FF3D00";
   public static final String color_blue = "#00E5FF";
}
