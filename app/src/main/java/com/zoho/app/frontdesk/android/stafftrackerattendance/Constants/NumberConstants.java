package com.zoho.app.frontdesk.android.stafftrackerattendance.Constants;

/**
 * Created by ArumugaLingam R on 28/01/20.
 */

public class NumberConstants {

    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This class is used to do hold constant values.
     * */
    public static final int kConstantZero=0;
    public static final Integer kConstantProduction = 02;
    public static Integer connectionTimeout = 1000 * 30;
    public static final Integer kConstantSuccessCode = 200;
}
