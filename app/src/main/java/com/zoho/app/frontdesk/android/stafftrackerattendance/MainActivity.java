package com.zoho.app.frontdesk.android.stafftrackerattendance;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zoho.app.frontdesk.android.stafftrackerattendance.Constants.MainConstants;
import com.zoho.app.frontdesk.android.stafftrackerattendance.backgroundProcess.service.BackgroundService;

public class MainActivity extends AppCompatActivity {

    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This activity is used to Handle background service and acts as home page
     * */
    private ImageView image_icon_back,image_icon_front;
    private TextView text_swipe,text_company_name;
    private boolean is_pic_stored = false;
    private Handler brightnessDimHandler;
    private Context context;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //here always keeping the screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
        }
        this.context = this;
        assign_object();
        //
        // here starting service instead of alarm manager
        startService(new Intent(this, BackgroundService.class));
    }
    //
    private void setAlarm() {
        Log.e("AM_SR*","*** Alarm manager is called  ***");
        try {
            Intent intent = new Intent(this, MyBroadcastReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    this.getApplicationContext(), 234324243, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            assert alarmManager != null;
            //alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntent);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), AlarmManager.INTERVAL_FIFTEEN_MINUTES, pendingIntent);
           // alarmManager.cancel(pendingIntent);

//            Intent intent = new Intent(this, MyService.class);
//            PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
//            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//            assert alarmManager != null;
//            alarmManager.cancel(pendingIntent);
//            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000, pendingIntent);
        }
        catch (Exception e){
            Log.e("AM_SR*"," *** Main service error~ ***"+e.getMessage());
        }

    }
    //
    private void assign_object()
    {
        //
        try{
            image_icon_back = findViewById(R.id.image_icon_back);
            image_icon_front = findViewById(R.id.image_icon_front);
            text_swipe = findViewById(R.id.text_swipe);
            text_company_name = findViewById(R.id.text_company_name);
            //
            image_icon_back.setImageResource(R.drawable.material_barcode_square_512);
            image_icon_front.setImageResource(R.drawable.ic_flash_on_white_24dp);
            text_swipe.setText("Application by");
            text_company_name.setText("CUBE YOGI");
            //
            final Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Calling scanner activity
                    show_employees();
                }

            }, 3000);
        }
        catch (Exception e)
        {
            Log.e("LOG***"," Exception : "+e.getMessage());
        }
    }
    //
    private void show_employees(){
        //
        Intent intent = new Intent(MainActivity.this,ActivityBarCodeScan.class);
        startActivity(intent);
        finish();
    }
    //
    @Override
    protected void onResume() {
        super.onResume();
        if (!NetworkConnection.isWiFiEnabled(getApplicationContext()))
        {
            NetworkConnection.wifiReconnect(getApplicationContext());
        }
    }
    //
    @Override
    protected void onStop() {
        super.onStop();
        if (brightnessDimHandler != null && brightnessDimCallback != null) {
            brightnessDimHandler.removeCallbacks(brightnessDimCallback);
        }
    }
    //
    private void setBrightnessDimTimer() {
        Log.e("DIM"," *** setBrightnessDimTimer ***");
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = (float)255;
        getWindow().setAttributes(lp);
        brightnessDimHandler.removeCallbacks(brightnessDimCallback);
        brightnessDimHandler.postDelayed(brightnessDimCallback,
                MainConstants.BRIGHTNESS_DIM_TIMEOUT);
    }
    //
    private Runnable brightnessDimCallback = new Runnable() {

        public void run() {
            Log.e("Run method","*** brightnessDimCallback is called ***");
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.screenBrightness = (float)0;
            getWindow().setAttributes(lp);
            // textViewManualEntryTitle.setTextColor(Color.parseColor("#FFFFFF"));
        }
    };

    //
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
    private void requestCameraPermission() {
        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // Display a SnackBar with cda button to request the missing permission.
            Toast.makeText(this, "Camera access is required to Scan The Barcode.",
                    Toast.LENGTH_LONG).show();

            // Request the permission
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);

            startActivity(getIntent());
            finish();

        } else {
            Toast.makeText(this,
                    "<b>Camera could not be opened.</b>", Toast.LENGTH_SHORT).show();
            // Request the permission. The result will be received in onRequestPermissionResult().
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        }
    }
}
