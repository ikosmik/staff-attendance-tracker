package com.zoho.app.frontdesk.android.stafftrackerattendance;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import com.zoho.app.frontdesk.android.stafftrackerattendance.Constants.MainConstants;
import com.zoho.app.frontdesk.android.stafftrackerattendance.backgroundProcess.storeEmployees;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.DataBaseRepository;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.EmployeeEntity;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.VisitorEntity;

import java.util.List;

public class MyBroadcastReceiver extends BroadcastReceiver {

    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This Service is used to do sync data with hostinger's mysql db.
     * */
    private Context context;
    private storeEmployees store_employees;
    private DataBaseRepository dataBaseRepository;

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;
            //
            try{
                if(checkConnectivity(context)) {
                    dataBaseRepository = new DataBaseRepository(context);
                    //
                    Log.e("AM_SR*", " *** status 0 : ***"+dataBaseRepository.is_rec_found_by_status(0));
                    Log.e("AM_SR*", " *** status 1 : ***"+dataBaseRepository.is_rec_found_by_status(1));
                    Log.e("AM_SR*", " *** status 2 : ***"+dataBaseRepository.is_rec_found_by_status(2));
                    Log.e("AM_SR*", " *** status 3 : ***"+dataBaseRepository.is_rec_found_by_status(3));
                    //
                    if(dataBaseRepository.is_rec_found_by_status(MainConstants.status_log_in))
                    {
                        store_employees = new storeEmployees(context);
                        Log.e("AM_SR*", " *** store is running 0 ***");
                    }
                    else if(dataBaseRepository.is_rec_found_by_status(MainConstants.status_log_out))
                    {
                        store_employees = new storeEmployees(context);
                        Log.e("AM_SR*", " *** store is running 1 ***");
                    }
                    //
                    Log.e("AM_SR*", " *** Alarm manager is running ***");
                }
                else{
                    Log.e("AM_SR*", " *** Alarm manager is not running no internet connection ***");
                }
            }
            catch (Exception e)
            {
                Log.e("AM_SR*"," *** receiver error~ ***"+e.getMessage());
            }

    }
    //
    //this function is used to check if network is available
    @NonNull
    public boolean checkConnectivity(Context context){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network network = connMgr.getActiveNetwork();
            if (network == null){
                return false;
            }
            NetworkCapabilities capabilities = connMgr.getNetworkCapabilities(network);
            if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                return true;
            }
            else if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                return true;
            }
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if(connectivityManager.getActiveNetworkInfo() != null){
                return connectivityManager.getActiveNetworkInfo().isConnected();
            }
            else{
                return false;
            }
        }
        return false;
    }
}
