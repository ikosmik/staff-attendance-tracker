package com.zoho.app.frontdesk.android.stafftrackerattendance;

public class VisitorSingleton {
    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This class is used to hold staff data while processing.
     * */
    private static VisitorSingleton Visitor = null;

    private String kConstantEmpty = "";
    private boolean Is_process_running;
    private boolean sync_running;

    public static VisitorSingleton getInstance() {
        if (Visitor == null) {
            Visitor = new VisitorSingleton();
        }
        return Visitor;
    }

    public VisitorSingleton()
    {
        //clearInstance();
    }

    public void clearInstance()
    {
        //this function will clear all singleton variable's data
        Is_process_running = false;
        sync_running = false;
    }

    public static VisitorSingleton getVisitor() {
        return Visitor;
    }

    public static void setVisitor(VisitorSingleton visitor) {
        Visitor = visitor;
    }

    public boolean is_process_running() {
        return Is_process_running;
    }

    public void set_is_process_running(boolean is_process_running) {
        Is_process_running = is_process_running;
    }

    public boolean isSync_running() {
        return sync_running;
    }

    public void setSync_running(boolean sync_running) {
        this.sync_running = sync_running;
    }
}
