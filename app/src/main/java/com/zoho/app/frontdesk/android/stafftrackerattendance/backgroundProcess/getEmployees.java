package com.zoho.app.frontdesk.android.stafftrackerattendance.backgroundProcess;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.zoho.app.frontdesk.android.stafftrackerattendance.Constants.MainConstants;
import com.zoho.app.frontdesk.android.stafftrackerattendance.Constants.NumberConstants;
import com.zoho.app.frontdesk.android.stafftrackerattendance.VisitorSingleton;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.DataBaseRepository;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class getEmployees {

    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This class is used to do sync staff data with hostinger mysql db using async class.
     * */
    DataBaseRepository dataBaseRepository;
    Context context;
    public getEmployees(Context context) {
        // here calling the background process class
        this.context = context;
        dataBaseRepository = new DataBaseRepository(context);
        if(!VisitorSingleton.getInstance().isSync_running()) {
            RunInBackground runInBackground = new RunInBackground();
            runInBackground.execute();
        }
    }
    //

    /**
     * Call GET or POST method Rest API call
     *
     * @param restApiMethod String
     * @param url           URL
     * @param setParameter  String
     * @return responseString String
     * @throws IOException
     * @author ArumugaLingam R
     * @created 21/08/20
     */
    public String restAPICall(String restApiMethod, URL url, String setParameter) throws IOException {
        String responseString = MainConstants.kConstantEmpty;
//        if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
//            Log.d("DSK RestapiMethod", "" + restApiMethod);
//            Log.d("DSK RestapiURL", "" + url + setParameter);
//        }
        if (restApiMethod != null && !restApiMethod.isEmpty() && url != null && !url.toString().isEmpty()) {
            HttpURLConnection con = null;

            con = (HttpURLConnection) url.openConnection();
            if (con != null) {
                con.setDoOutput(true);
                con.setRequestMethod(restApiMethod);
                con.setConnectTimeout(NumberConstants.connectionTimeout);
                con.setReadTimeout(NumberConstants.connectionTimeout);

                if (restApiMethod == MainConstants.kConstantPostMethod && setParameter != null && !setParameter.isEmpty()) {
                    con.setRequestProperty(MainConstants.kConstantUserAgent,
                            MainConstants.user_Agent);
                    con.setRequestProperty(MainConstants.kConstantAcceptLanguage,
                            MainConstants.acceptLanguage);
                    DataOutputStream dataOutputStream = new DataOutputStream(
                            con.getOutputStream());
                    if (dataOutputStream != null) {
                        dataOutputStream.writeBytes(setParameter);
                        dataOutputStream.flush();
                        dataOutputStream.close();
                    }
                } else {
//                    con.connect();
                }
                if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
                    Log.d("DSK restApiStatus ", "message " + con.getResponseMessage() + "");
                }
                int status = con.getResponseCode();
                if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
                    Log.d("DSK restApiStatusCode", status + "");
                }
                if (status == NumberConstants.kConstantSuccessCode) {
                    // read the response
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(con.getInputStream()));

                    StringBuffer response = new StringBuffer();
                    String line = null;
                    while ((line = in.readLine()) != null) {
                        response.append(line + MainConstants.kConstantNewLine);
                    }
                    responseString = response.toString();
                    if (NumberConstants.kConstantProduction == NumberConstants.kConstantZero) {
                        Log.d("DSK ", "response:" + responseString);
                    }
                }
            }
        }
        return responseString;
    }
    //
    public void getShiftDetails(){
        try
        {
            // here forming the url
//                String getUrl = MainConstants.get_url;
            // http://awesomesimple.96.lt/staff_time_tracker/get_st_employees.php
            String getUrl = "http://awesomesimple.96.lt/staff_time_tracker/get_st_shifts.php";
            URL url = new URL(getUrl);
            String response = restAPICall(MainConstants.kConstantGetMethod,url,"");
            //response = response.substring(28,response.length()-1);
            // here getting response as string
            Log.e("Log"," ***Get URL Shift info*** : "+getUrl);
            Log.e("Log"," ***Get Shift response*** : "+response);
            // here parsing the json data
            // JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = new JSONArray(response);
            // here getting employee details from json array
            for (int i = 0; i < jsonArray.length(); i++) {

                //[{"shift_id":"100","shift_name":"Morning shift","start_time":"06:00:00","end_time":"17:00:00","is_active":"1"}]

                JSONObject jsonObject_keys = jsonArray.getJSONObject(i);
                String creator_id = "ID";
                //String creator_id = jsonObject_keys.getString("ID");
                String shift_id = jsonObject_keys.getString("shift_id");
                String shift_name = jsonObject_keys.getString("shift_name");
                String start_time = jsonObject_keys.getString("start_time");
                String end_time = jsonObject_keys.getString("end_time");
                int is_active = jsonObject_keys.getInt("is_active");
                Log.e("Log"," *** shift *** : "+shift_name);
                boolean isActive;

                if(is_active == 1){
                    isActive = true;
                }
                else{
                    isActive = false;
                }
                // here checking employee is available or not
                if(dataBaseRepository.is_shift_available(shift_id)){
                    if(isActive) {
                        // if employee is found update employee
                        Log.e("Log"," *** Shift Updated *** : "+shift_name);
                        dataBaseRepository.updateWorkShift(shift_id,shift_name,start_time,end_time,is_active);
                    }
                    else{
                        // if employee is not active delete employee
                        Log.e("Log"," *** Shift Deleted *** : "+shift_name);
                        // TODO delete shift
                        dataBaseRepository.deleteShiftRecord(shift_id);
                    }
                }
                else
                {
                    // if employee is not found add employee
                    Log.e("Log"," *** Shift Added *** : "+shift_name);
                    dataBaseRepository.addWorkShift(shift_id,shift_name,start_time,end_time,is_active);
                }

            }
        }
        catch (Exception e){
            // here printing error in log
            Log.e("Log"," ***Get Method Error Shift info *** : "+e.getMessage());
        }
    }
    //
    @SuppressLint("StaticFieldLeak")
    class RunInBackground extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            try
            {
                VisitorSingleton.getInstance().setSync_running(true);
              // here forming the url
//                String getUrl = MainConstants.get_url;
                // http://awesomesimple.96.lt/staff_time_tracker/get_st_employees.php
                String getUrl = "http://awesomesimple.96.lt/staff_time_tracker/get_st_employees.php";
                URL url = new URL(getUrl);
                String response = restAPICall(MainConstants.kConstantGetMethod,url,"");
                //response = response.substring(28,response.length()-1);
                // here getting response as string
                Log.e("Log"," ***Get URL*** : "+getUrl);
                Log.e("Log"," ***Get Method response*** : "+response);
                // here parsing the json data
               // JSONObject jsonObject = new JSONObject(response);
                JSONArray jsonArray = new JSONArray(response);
                // here getting employee details from json array
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject_keys = jsonArray.getJSONObject(i);
                    String creator_id = "ID";
                    //String creator_id = jsonObject_keys.getString("ID");
                    String employee_id = jsonObject_keys.getString("staff_id");
                    String employee_name = jsonObject_keys.getString("staff_name");
                    String phone_number = jsonObject_keys.getString("contact_no");
                    String is_active = jsonObject_keys.getString("is_active");
                    String login_code = jsonObject_keys.getString("login_code");
                    Log.e("Log"," *** Employee *** : "+employee_name);
                    boolean isActive;
                    boolean isAdded;
                    if(is_active.equalsIgnoreCase("1")){
                        isActive = true;
                    }
                    else{
                        isActive = false;
                    }
                    // here checking employee is avialable or not
                    if(dataBaseRepository.is_employee_available(employee_id)){
                        if(isActive) {
                            // if employee is found update employee
                            Log.e("Log"," *** Employee Updated *** : "+employee_name);
                            dataBaseRepository.updateEmployees(creator_id,employee_id,employee_name,phone_number,isActive,login_code);
                        }
                        else{
                            // if employee is not active delete employee
                            Log.e("Log"," *** Employee Deleted *** : "+employee_name);
                            dataBaseRepository.deleteEmployee(employee_id);
                        }
                    }
                    else
                        {
                            // if employee is not found add employee
                        Log.e("Log"," *** Employee Added *** : "+employee_name);
                        dataBaseRepository.addEmployees(creator_id, employee_id, employee_name, phone_number, isActive,login_code);
                    }

                }
                // here getting shift info
                getShiftDetails();
                VisitorSingleton.getInstance().setSync_running(false);
            }
            catch (Exception e){
                // here printing error in log
                Log.e("Log"," ***Get Method Error*** : "+e.getMessage());
                VisitorSingleton.getInstance().setSync_running(false);
            }
            return null;
        }
    }
}
