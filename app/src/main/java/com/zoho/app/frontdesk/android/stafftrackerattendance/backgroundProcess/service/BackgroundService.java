package com.zoho.app.frontdesk.android.stafftrackerattendance.backgroundProcess.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zoho.app.frontdesk.android.stafftrackerattendance.Constants.MainConstants;
import com.zoho.app.frontdesk.android.stafftrackerattendance.VisitorSingleton;
import com.zoho.app.frontdesk.android.stafftrackerattendance.backgroundProcess.getEmployees;
import com.zoho.app.frontdesk.android.stafftrackerattendance.backgroundProcess.storeEmployees;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.DataBaseRepository;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.ShiftsEntity;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.VisitorEntity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class BackgroundService extends Service {
    //creating a service
    private Context context;
    private storeEmployees store_employees;
    private getEmployees get_employees;
    private Timer timerObj = new Timer();
    private int time_in_mills_15_minutes = 900000;
    //private int time_in_mills_15_minutes = 10000;
    //
    private Timer timer_Obj_one_hour = new Timer();
    private int time_in_mills_one_hour = 3600000;
    private DataBaseRepository dataBaseRepository;

    /**
     //starting service
     startService(new Intent(this, BackgroundService.class));

     //stopping service
     stopService(new Intent(this, BackgroundService.class));
     */


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //TODO add the service code here
        this.context = this;
        start_timer();
        //shift_log_out_timer();
        //start sticky means service will be explicitly started and stopped
        return START_STICKY;
    }

    //
   public void show_toast(){
       Toast.makeText(context,"SAT service : "+get_current_time(),Toast.LENGTH_LONG).show();
   }
    //this function is used to show progress while bg process is running
    public void start_timer(){
        TimerTask timerTaskObj = new TimerTask() {
            public void run() {
                //TODO write code here
                try{
                    Log.e("BG_SR*"," *** is running ***"+get_current_time());

                    if(checkConnectivity(context)) {
                        //
                        get_employees = new getEmployees(context);
                        //
                        dataBaseRepository = new DataBaseRepository(context);
                        //
                        Log.e("BG_SR*", " *** status 0 : ***"+dataBaseRepository.is_rec_found_by_status(0));
                        Log.e("BG_SR*", " *** status 1 : ***"+dataBaseRepository.is_rec_found_by_status(1));
                        Log.e("BG_SR*", " *** status 2 : ***"+dataBaseRepository.is_rec_found_by_status(2));
                        Log.e("BG_SR*", " *** status 3 : ***"+dataBaseRepository.is_rec_found_by_status(3));
                        //
                        if(dataBaseRepository.is_rec_found_by_status(MainConstants.status_log_in))
                        {
                            store_employees = new storeEmployees(context);
                            Log.e("BG_SR*", " *** store is running 0 ***");
                        }
                        else if(dataBaseRepository.is_rec_found_by_status(MainConstants.status_log_out))
                        {
                            store_employees = new storeEmployees(context);
                            Log.e("BG_SR*", " *** store is running 1 ***");
                        }
                        //
                        Log.e("BG_SR*", " *** Background Service is running ***");
                    }
                    else{
                        Log.e("BG_SR*", " *** Background Service is not running no internet connection ***");
                    }
                }
                catch (Exception e)
                {
                    Log.e("BG_SR*"," *** receiver error~ ***"+e.getMessage());
                }
                
            }
        };
        timerObj.schedule(timerTaskObj, 0, time_in_mills_15_minutes);
    }


    //this function is used to check if network is available
    @NonNull
    public boolean checkConnectivity(Context context){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network network = connMgr.getActiveNetwork();
            if (network == null){
                return false;
            }
            NetworkCapabilities capabilities = connMgr.getNetworkCapabilities(network);
            if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                return true;
            }
            else if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                return true;
            }
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if(connectivityManager.getActiveNetworkInfo() != null){
                return connectivityManager.getActiveNetworkInfo().isConnected();
            }
            else{
                return false;
            }
        }
        return false;
    }

    //this function is used to show progress while bg process is running
    public void shift_log_out_timer(){
//        TimerTask timerTaskObj = new TimerTask() {
//            public void run() {
//                //TODO write code here
//                Log.e("tag","*** log_out_employees timer running ***");
//                log_out_employees();
//            }
//        };
//        timer_Obj_one_hour.schedule(timerTaskObj, 0, time_in_mills_one_hour);
    }

    //
    public void log_out_employees()
    {
//        try
//        {
//            Log.e("tag","*** log_out_employees function running ***");
//            SimpleDateFormat dateFormat = new SimpleDateFormat("HH");
//            String formattedDate = dateFormat.format(new Date());
//            int current_hour = Integer.parseInt(formattedDate);
//            //
//            List<ShiftsEntity> shiftsEntities;
//            shiftsEntities = dataBaseRepository.fetchWorkShift(1);
//            for (int i = 0; i < shiftsEntities.size(); i++) {
//
//                String start_time = shiftsEntities.get(i).getEnd_time();
//                int shift_start_time = Integer.parseInt(start_time.substring(0,2));
//
//                String end_time = shiftsEntities.get(i).getEnd_time();
//                int shift_end_time = Integer.parseInt(end_time.substring(0,2));
//
//                if(current_hour == shift_end_time){
//                    //
//                    auto_log_out_employee(shift_start_time,shift_end_time);
//                    // TODO set log out time to all employees as current time
//                }
//            }
//        }
//        catch (Exception e)
//        {
//            Log.e("tag","*** log_out_employees error ***"+e.getMessage());
//        }
    }
    //
    public void auto_log_out_employee(int start_time,int end_time){
//        try
//        {
//            Log.e("tag","*** log_out_employees function running ***");
//            SimpleDateFormat dateFormat = new SimpleDateFormat("HH");
//            String formattedDate = dateFormat.format(new Date());
//            int current_hour = Integer.parseInt(formattedDate);
//            //
//            List<VisitorEntity> list_visitor_entity;
//            VisitorEntity visitor_entity;
//            list_visitor_entity = dataBaseRepository.fetchAllAttendance(get_current_Date());
//            for (int i = 0; i < list_visitor_entity.size(); i++) {
//                //
//                String log_in_time = list_visitor_entity.get(i).getLog_in_time();
//                int log_in_hour = Integer.parseInt(log_in_time.substring(0,2));
//                //
//                if(list_visitor_entity.get(i).getEmployee_id() != null && list_visitor_entity.get(i).getLog_in_time() !=null && list_visitor_entity.get(i).getLog_out_time().isEmpty()){
//                    //
//                    if(start_time >= log_in_hour && end_time < log_in_hour){
//                        //
//                        dataBaseRepository.update_attendance_record(list_visitor_entity.get(i).getId(), get_current_time(),MainConstants.status_log_out);
//                    }
//                }
//            }
//        }
//        catch (Exception e)
//        {
//            Log.e("tag","*** log_out_employees error ***"+e.getMessage());
//        }
    }
    //
    public String get_current_Date()
    {
        //here creating a calendar object
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat(MainConstants.json_time_pattern);
            String formattedDate = dateFormat.format(new Date());
            return formattedDate;
        }
        catch (Exception e)
        {
            return "";
        }
    }
    //
    public String get_current_time()
    {
        //here creating a calendar object
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat(MainConstants.app_date_time_pattern);
            String formattedDate = dateFormat.format(new Date());
            return formattedDate;
        }
        catch (Exception e)
        {
            return "";
        }
    }
    //
    @Override
    public void onDestroy() {
        super.onDestroy();
        // service is destroyed
        timerObj.cancel();
        timerObj.purge();
        //
//        timer_Obj_one_hour.cancel();
//        timer_Obj_one_hour.purge();
    }
}