package com.zoho.app.frontdesk.android.stafftrackerattendance.backgroundProcess;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.zoho.app.frontdesk.android.stafftrackerattendance.Constants.MainConstants;
import com.zoho.app.frontdesk.android.stafftrackerattendance.Constants.NumberConstants;
import com.zoho.app.frontdesk.android.stafftrackerattendance.MainActivity;
import com.zoho.app.frontdesk.android.stafftrackerattendance.VisitorSingleton;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.DataBaseRepository;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.EmployeeEntity;
import com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB.VisitorEntity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.net.Uri.encode;

public class storeEmployees {

    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This class is used to do send staff data to hostinger's mysql db.
     * */
    DataBaseRepository dataBaseRepository;
    private List<VisitorEntity> employee_list;
    private ArrayList<String> employee_update_list = new ArrayList<>();
    private ArrayList<String> employee_delete_list = new ArrayList<>();
    final String json_time_pattern = "dd-MMM-yyyy";
    Context context;
    //
    public storeEmployees(Context context) {
        //
        this.context = context;
        dataBaseRepository = new DataBaseRepository(context);
        if(!VisitorSingleton.getInstance().is_process_running()){
            //
            RunInBackground runInBackground = new RunInBackground();
            runInBackground.execute();
        }
    }
    //

    /**
     * Call GET or POST method Rest API call
     *
     * @param restApiMethod String
     * @param url           URL
     * @param setParameter  String
     * @return responseString String
     * @throws IOException
     * @author ArumugaLingam R
     * @created 21/08/20
     */
    public String restAPICall(String restApiMethod, URL url, String setParameter) throws IOException {
        String responseString = MainConstants.kConstantEmpty;
        Log.e("DSK RestapiMethod", "" + restApiMethod);
        Log.e("DSK RestapiURL", "" + url + setParameter);
        if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
            Log.e("DSK RestapiMethod", "" + restApiMethod);
            Log.e("DSK RestapiURL", "" + url + setParameter);
        }
        if (restApiMethod != null && !restApiMethod.isEmpty() && url != null && !url.toString().isEmpty()) {
            HttpURLConnection con = null;

            con = (HttpURLConnection) url.openConnection();
            if (con != null) {
                con.setDoOutput(true);
                con.setRequestMethod(restApiMethod);
                con.setConnectTimeout(NumberConstants.connectionTimeout);
                con.setReadTimeout(NumberConstants.connectionTimeout);

                if (restApiMethod == MainConstants.kConstantPostMethod && setParameter != null && !setParameter.isEmpty()) {
                    con.setRequestProperty(MainConstants.kConstantUserAgent,
                            MainConstants.user_Agent);
                    con.setRequestProperty(MainConstants.kConstantAcceptLanguage,
                            MainConstants.acceptLanguage);
                    DataOutputStream dataOutputStream = new DataOutputStream(
                            con.getOutputStream());
                    if (dataOutputStream != null) {
                        dataOutputStream.writeBytes(setParameter);
                        dataOutputStream.flush();
                        dataOutputStream.close();
                    }
                } else {
//                    con.connect();
                }
                if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
                    Log.e("DSK restApiStatus ", "message " + con.getResponseMessage() + "");
                }
                int status = con.getResponseCode();
                if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
                    Log.e("DSK restApiStatusCode", status + "");
                }
                if (status == NumberConstants.kConstantSuccessCode) {
                    // read the response
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(con.getInputStream()));

                    StringBuffer response = new StringBuffer();
                    String line = null;
                    while ((line = in.readLine()) != null) {
                        response.append(line + MainConstants.kConstantNewLine);
                    }
                    responseString = response.toString();
                    if (NumberConstants.kConstantProduction == NumberConstants.kConstantZero) {
                        Log.e("DSK ", "response:" + responseString);
                    }
                }
            }
        }
        return responseString;
    }
    //
    class RunInBackground extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            try
            {
                VisitorSingleton.getInstance().set_is_process_running(true);
                //
                String json_data_post = form_json();
                String postUrl = "http://awesomesimple.96.lt/staff_time_tracker/add_app_data.php?request_json="+encode(json_data_post);
                //String postUrl = MainConstants.post_url+encode(json_data_post);

                URL url = new URL(postUrl);
                String response = restAPICall(MainConstants.kConstantPostMethod,url,"");
                //response = response.substring(28,response.length()-1);
                //
                Log.e("Log"," ***Post URL*** : "+postUrl);
                Log.e("Log"," ***Post Method response*** : "+response);

                // {"status_code":"1",\"message\":"."\"New record created successfully !\""."}
                if(response != null && !response.isEmpty()) {

                    JSONObject json_response = new JSONObject(response);
                    if (json_response != null && !json_response.toString().isEmpty()) {
                        // TODO update status
                        if (json_response.get("status_code").equals("1")) {
                            // TODO remove uploaded records
                            update_record_status();
                            VisitorSingleton.getInstance().set_is_process_running(false);
                        } else {
                            // TODO show error
                            VisitorSingleton.getInstance().set_is_process_running(false);
                        }
                    }
                }
                //
            }
            catch (Exception e){
                Log.e("Log"," ***Post Method Error*** : "+e.getMessage());
                VisitorSingleton.getInstance().set_is_process_running(false);
            }
            return null;
        }
        //
        public String get_current_Date()
        {
            //here creating a calendar object
            try
            {
                SimpleDateFormat dateFormat = new SimpleDateFormat(json_time_pattern);
                String formattedDate = dateFormat.format(new Date());
                return formattedDate;
            }
            catch (Exception e)
            {
                return "";
            }
        }
        //
        private String form_json()
        {
            String formed_json_data;
            JSONObject jsonObject_data = new JSONObject();
            JSONObject jsonObject_staff = new JSONObject();
            JSONArray jsonArray_staff = new JSONArray();
            String log_out = "";
            String log_in = "";
            //
            try{
                if(dataBaseRepository != null) {
                    employee_list = dataBaseRepository.fetchAllAttendance(get_current_Date());
                    String last_logged_out = "";
                    for (int i = 0; i < employee_list.size(); i++) {
                        Log.e("Log***", " *** Form json status *** : " + employee_list.get(i).getStatus());
                        //
                        if (employee_list.get(i).getStatus() < MainConstants.status_login_sent) {

                            if (employee_list.get(i).getLog_out_time() != null && employee_list.get(i).getLog_in_time() != null
                                    && !employee_list.get(i).getLog_out_time().isEmpty() && !employee_list.get(i).getLog_in_time().isEmpty()) {
                                log_out = employee_list.get(i).getLog_out_time();
                                log_in = employee_list.get(i).getLog_in_time();
                                //
                                jsonObject_data.put("staff_name", employee_list.get(i).getEmployee_name());
                                jsonObject_data.put("staff_id", employee_list.get(i).getEmployee_id());
                                jsonObject_data.put("staff_shift", "");
                                jsonObject_data.put("staff_late", "");
                                jsonObject_data.put("staff_in_time", log_in);
                                jsonObject_data.put("staff_out_time", log_out);
                                jsonObject_data.put("status", employee_list.get(i).getStatus());
                                jsonObject_data.put("shift_id", employee_list.get(i).getShift_id());
                                //
                                JSONObject jsonObject_key = new JSONObject();
                                jsonObject_key.put("json_data",jsonObject_data);
                                jsonArray_staff.put(jsonObject_key);
                                //
                                employee_delete_list.add(employee_list.get(i).getEmployee_id());
                            }
                            else if (employee_list.get(i).getLog_in_time() != null && !employee_list.get(i).getLog_in_time().isEmpty()) {
                                log_out = "";
                                log_in = employee_list.get(i).getLog_in_time();
                                //
                                jsonObject_data.put("staff_name", employee_list.get(i).getEmployee_name());
                                jsonObject_data.put("staff_id", employee_list.get(i).getEmployee_id());
                                jsonObject_data.put("staff_shift", "");
                                jsonObject_data.put("staff_late", "");
                                jsonObject_data.put("staff_in_time", log_in);
                                jsonObject_data.put("staff_out_time", log_out);
                                jsonObject_data.put("status", employee_list.get(i).getStatus());
                                //
                                JSONObject jsonObject_key = new JSONObject();
                                jsonObject_key.put("json_data",jsonObject_data);
                                jsonArray_staff.put(jsonObject_key);
                                employee_update_list.add(employee_list.get(i).getEmployee_id());
                            } else {
                                // TODO delete records
                            }
                        }
                    }
                }
                jsonObject_staff.put("json_data",jsonArray_staff);
                //
                formed_json_data = jsonArray_staff.toString();
                Log.e("Log"," *** Form json data *** : "+formed_json_data);
            }
            catch (Exception e)
            {
                formed_json_data = "";
                //
                Log.e("Log"," *** Form json error *** : "+e.getMessage());
            }
            return formed_json_data;
        }
        //
        private void update_record_status()
        {
            //
            try{
                if(dataBaseRepository != null) {
                    //
                    Log.e("Log"," *** employee_update_list *** : "+employee_update_list);
                    Log.e("Log"," *** employee_delete_list *** : "+employee_delete_list);
                    for (int j = 0; j < employee_update_list.size(); j++) {
                        // TODO  updating records
                        dataBaseRepository.update_attendance_status(employee_update_list.get(j),2);
                    }
                    for (int i = 0; i < employee_delete_list.size() ; i++) {
                        // TODO delete records instead of updating records
                        dataBaseRepository.update_attendance_status(employee_delete_list.get(i),3);
                    }
                }
            }
            catch (Exception e)
            {
                Log.e("Log"," *** update_record_status error *** : "+e.getMessage());
            }
        }
    }
}

