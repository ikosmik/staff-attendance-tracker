package com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB;

import android.content.Context;

import androidx.room.Room;

import com.zoho.app.frontdesk.android.stafftrackerattendance.backgroundProcess.storeEmployees;

import java.util.List;

public class DataBaseRepository {

    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This class will allow the application to access room database.
     * */
    final String database_name = "visitor_db";
    final String emp_database_name = "employee_db";
    final String shift_database_name = "work_shifts_db";
    private AppDatabase appDatabase;
    private AppDatabase shiftDatabase;
    private EmployeeAppDatabase employeeAppDatabase;
    private EmployeeEntity employeeEntity = new EmployeeEntity();
    private VisitorEntity entity = new VisitorEntity();
    private ShiftsEntity shiftEntity = new ShiftsEntity();
    private Context context;
    private storeEmployees store_employees;
    // here creating a constructor for this class for calling from other activities
    public DataBaseRepository(Context context)
    {
        this.context = context;
        //here creating the database
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, database_name).allowMainThreadQueries().build();
        employeeAppDatabase = Room.databaseBuilder(context, EmployeeAppDatabase.class, emp_database_name).allowMainThreadQueries().build();
        shiftDatabase = Room.databaseBuilder(context, AppDatabase.class, shift_database_name).allowMainThreadQueries().build();

    }
    // This function is used to insert record into Room DB table
    public void insert_attendance_record(String employee_id,String employee_name,
                              String log_in_time,String log_out_time,
                              String image_location, int attendance_count,int status,String current_date,String shift_id){
        entity.setEmployee_id(employee_id);
        entity.setEmployee_name(employee_name);
        entity.setLog_in_time(log_in_time);
        entity.setLog_out_time(log_out_time);
        entity.setImage_location(image_location);
        entity.setStatus(status);
        entity.setAttendance_count(attendance_count);
        entity.setToday_date(current_date);
        entity.setShift_id(shift_id);
        // here adding the record
        appDatabase.appDatabaseObject().addRecord(entity);
    }
    // This function is used to update record into Room DB table
    public void update_attendance_record(int record_id,String log_out_time,int status){
        //
        appDatabase.appDatabaseObject().updateVisitorRecord(record_id,log_out_time,status);
    }
    //
    // This function is used to update record into Room DB table
    public void update_attendance_status(String employee_id,int status){
        //
        appDatabase.appDatabaseObject().updateVisitorRecordStatus(employee_id,status);
    }
    // This function is used to delete records from Room DB table
    public void delete_visitor_records(){

    }
    //
    //
    public void addEmployees(String creator_id,String employee_id,String employee_name,String phone_number,boolean is_active,String login_code)
    {
        employeeEntity.setCreator_id(creator_id);
        employeeEntity.setEmployee_id(employee_id);
        employeeEntity.setEmployee_name(employee_name);
        employeeEntity.setPhone_number(phone_number);
        employeeEntity.setLogin_code(login_code);
        if(is_active){
            employeeEntity.setIs_active("true");
        }
        else{
            employeeEntity.setIs_active("false");
        }
        employeeAppDatabase.employeeAppDatabase().addRecord(employeeEntity);
    }
    //
    public List<EmployeeEntity> fetchEmployees(){

        return employeeAppDatabase.employeeAppDatabase().getAllEmployee();
    }
    //
    public List<EmployeeEntity> fetchEmployeesByLoginCode(String login_code){

        return employeeAppDatabase.employeeAppDatabase().getRecordFromLoginID(login_code);
    }
    //
    public List<EmployeeEntity> fetchEmployeesByID(String employee_id){

        return employeeAppDatabase.employeeAppDatabase().getRecordsByEmployeeID(employee_id);
    }
    //
    public List<VisitorEntity> fetchAttendance(String employee_id,String current_date){

        return appDatabase.appDatabaseObject().getVisitorRecord(employee_id,current_date);
    }
    //
    public List<VisitorEntity> fetchAllAttendance(String current_date){

        return appDatabase.appDatabaseObject().getAllVisitorRecords(current_date);
    }
    //
    public List<VisitorEntity> fetchInCompleteAttendance(String employee_id,String current_date){

        return appDatabase.appDatabaseObject().getIncompleteRecord(employee_id,current_date);
    }
    //
    public List<VisitorEntity> fetchCompleteAttendance(String employee_id,String current_date){

        return appDatabase.appDatabaseObject().getCompleteRecord(employee_id,current_date);
    }
    //
    public boolean is_attendance_found(String employee_id,String current_date){
        int count =0;
        //
        count = appDatabase.appDatabaseObject().isAttendanceFound(employee_id,current_date);
        if(count > 0){
            return true;
        }
        else{

            return false;
        }
    }
    //
    //
    public boolean is_rec_found_by_status(int status){
        int count =0;
        //
        count = appDatabase.appDatabaseObject().isRecordFoundByStatus(status);
        if(count > 0){
            return true;
        }
        else{

            return false;
        }
    }
    //
    public void updateEmployees(String creator_id,String employee_id,String employee_name,String phone_number,boolean isActive,String login_code){

        String is_active = "";
        if(isActive){
            is_active = "true";
        }
        else{
            is_active = "false";
        }
        employeeAppDatabase.employeeAppDatabase().updateEmployeeByID(creator_id,employee_id,employee_name,phone_number,is_active,login_code);
    }
    //
    public void deleteEmployee(String employee_id){
        employeeAppDatabase.employeeAppDatabase().deleteEmployeeByID(employee_id);
    }
    //
    public boolean is_employee_available(String employee_id){
        //
        int found = employeeAppDatabase.employeeAppDatabase().isEmployeeFound(employee_id);
        if(found > 0){
            return true;
        }
        else{
            return false;
        }
    }
    //
    public void addWorkShift(String shift_id,String shift_name,String start_time,String end_time,int is_active)
    {
        shiftEntity.setShift_id(shift_id);
        shiftEntity.setShift_name(shift_name);
        shiftEntity.setStart_time(start_time);
        shiftEntity.setEnd_time(end_time);
        shiftEntity.setIs_active(is_active);
        //
        shiftDatabase.appDatabaseObject().addShiftRecord(shiftEntity);
    }
    //
    public void updateWorkShift(String shift_id,String shift_name,String start_time,String end_time,int is_active)
    {
        shiftEntity.setShift_id(shift_id);
        shiftEntity.setShift_name(shift_name);
        shiftEntity.setStart_time(start_time);
        shiftEntity.setEnd_time(end_time);
        shiftEntity.setIs_active(is_active);
        //
        shiftDatabase.appDatabaseObject().updateShiftRecord(shiftEntity);
    }
    //
    public List<ShiftsEntity> fetchWorkShift(int is_active){

        return appDatabase.appDatabaseObject().fetchWorkShifts(is_active);
    }
    //
    public boolean is_shift_available(String shift_id){
        //
        int found = shiftDatabase.appDatabaseObject().isShiftRecordFound(shift_id);
        if(found > 0){
            return true;
        }
        else{
            return false;
        }
    }
    //
    public void deleteShiftRecord(String shift_id){
        //
        shiftDatabase.appDatabaseObject().deleteShiftRecordByID(shift_id);
    }
}
