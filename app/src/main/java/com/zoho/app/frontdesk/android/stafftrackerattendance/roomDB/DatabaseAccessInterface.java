package com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DatabaseAccessInterface {

    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This class is used to query room database tables.
     * */
    // here using the table name
    final String visitor_table_name= "visitor_db";
    final String shift_table_name= "work_shifts_db";

    // query to insert records
    @Insert
    void addRecord(VisitorEntity entity);
    // query to update records

    @Update
    void updateRecord(VisitorEntity entity);
    // query to delete records

    @Delete
    void deleteRecord(VisitorEntity entity);

    // query to fetch records
    @Query("SELECT * FROM "+visitor_table_name+" where id=:id")
    List<VisitorEntity> getRecordByID(String id);

    // query to fetch employee from id
    @Query("SELECT * FROM "+visitor_table_name+" where employee_id=:id")
    List<VisitorEntity> getRecordFromEmpID(String id);

    // query to fetch employee attendance from employee id and date
    @Query("SELECT * FROM "+visitor_table_name+" where employee_id=:employee_id AND today_date=:current_date ORDER BY attendance_count asc")
    List<VisitorEntity> getVisitorRecord(String employee_id, String current_date);

    // query to fetch all employee attendance from date
    @Query("SELECT * FROM "+visitor_table_name+" where today_date=:current_date ORDER BY id asc")
    List<VisitorEntity> getAllVisitorRecords(String current_date);

    // query to check is employee found or not
    @Query("SELECT COUNT(employee_id) FROM "+visitor_table_name+" where employee_id=:employee_id AND today_date=:current_date")
    int isAttendanceFound(String employee_id, String current_date);

    // query to check employee records found or not by particular status
    @Query("SELECT COUNT(employee_id) FROM "+visitor_table_name+" where status=:status")
    int isRecordFoundByStatus(int status);

    // query to
    @Query("SELECT * FROM "+visitor_table_name+" where employee_id=:employee_id AND today_date=:current_date AND log_in_time!='' AND log_out_time='' ")
    List<VisitorEntity> getIncompleteRecord(String employee_id, String current_date);

    //
    @Query("SELECT * FROM "+visitor_table_name+" where employee_id=:employee_id AND today_date=:current_date AND log_in_time!='' AND log_out_time!='' ")
    List<VisitorEntity> getCompleteRecord(String employee_id, String current_date);

    //
    @Query("UPDATE "+visitor_table_name+" SET log_out_time=:log_out_time, status=:status where id=:id")
    void updateVisitorRecord(int id, String log_out_time,int status);

    //
    @Query("UPDATE "+visitor_table_name+" SET status=:status where employee_id=:id")
    void updateVisitorRecordStatus(String id, int status);

    /** Here adding queries for work shifts */

    // query to insert records
    @Insert
    void addShiftRecord(ShiftsEntity entity);

    // query to update records
    @Update
    void updateShiftRecord(ShiftsEntity entity);

    // query to delete records
    @Delete
    void deleteShiftRecord(ShiftsEntity entity);

    // query to fetch records
    @Query("SELECT * FROM "+shift_table_name+" where is_active=:is_active")
    List<ShiftsEntity> fetchWorkShifts(int is_active);

    //
    @Query("SELECT COUNT(shift_id) FROM "+shift_table_name+" where shift_id = :id")
    int isShiftRecordFound(String id);

    //
    @Query("DELETE FROM "+shift_table_name+" WHERE shift_id=:id")
    void deleteShiftRecordByID(String id);

    // query to fetch records
    @Query("UPDATE "+shift_table_name+" SET shift_id=:shift_id,shift_name=:shift_name,start_time=:start_time,end_time=:end_time,is_active=:is_active where shift_id = :shift_id")
    void updateShiftRecordByID(String shift_id, String shift_name, String start_time, String end_time, int is_active);

}
