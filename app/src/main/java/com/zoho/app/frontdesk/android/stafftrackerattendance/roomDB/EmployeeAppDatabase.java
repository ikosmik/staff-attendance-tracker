package com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = EmployeeEntity.class, version = 1, exportSchema = false)
public abstract class EmployeeAppDatabase extends RoomDatabase {
    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This class is used create object to acces room db.
     * */
    // here creating object to access dao class
    public abstract EmployeeDatabaseAccessInterface employeeAppDatabase();
}
