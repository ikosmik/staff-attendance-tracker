package com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface EmployeeDatabaseAccessInterface
{
    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This class is used to query room database tables.
     * */
    // here using the table name
    final String table_name = "employee_db";

    // query to insert records
    @Insert
    void addRecord(EmployeeEntity entity);

    // query to update records
    @Update
    void updateRecord(EmployeeEntity entity);

    // query to delete records
    @Delete
    void deleteRecord(EmployeeEntity entity);

    // query to fetch records
    @Query("SELECT * FROM "+table_name+" where employee_id = :employee_id")
    List<EmployeeEntity> getRecordsByEmployeeID(String employee_id);

    //
    @Query("SELECT * FROM "+table_name+" where login_code = :id")
    List<EmployeeEntity> getRecordFromLoginID(String id);

    //
    @Query("SELECT * FROM "+table_name)
    List<EmployeeEntity> getAllEmployee();

    //
    @Query("SELECT COUNT(employee_id) FROM "+table_name+" where employee_id = :id")
    int isEmployeeFound(String id);
    //
    @Query("DELETE FROM "+table_name+" WHERE employee_id=:employee_id")
    void deleteEmployeeByID(String employee_id);

    // query to fetch records
    @Query("UPDATE "+table_name+" SET creator_id=:creator_id,employee_name=:employee_name,phone_number=:phone_number,is_active=:is_active,login_code=:login_code where employee_id = :employee_id")
    void updateEmployeeByID(String creator_id, String employee_id, String employee_name, String phone_number, String is_active, String login_code);

}