package com.zoho.app.frontdesk.android.stafftrackerattendance.roomDB;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

// here setting table name
@Entity(tableName="work_shifts_db")
public class ShiftsEntity {

    /*
     * @created      : Arumugalingam | CubeYogi
     * @created on   : 16/10/2020
     * @created for  : This class is a room database entity created to store employee shift records.
     * */

    // here adding record id as auto generate id
    @PrimaryKey(autoGenerate = true)
    private int id;

    // here adding employee_id
    @ColumnInfo(name = "shift_id")
    private String shift_id;

    // here adding employee_id
    @ColumnInfo(name = "shift_name")
    private String shift_name;

    // here adding employee_id
    @ColumnInfo(name = "start_time")
    private String start_time;

    // here adding employee_id
    @ColumnInfo(name = "end_time")
    private String end_time;

    // here adding employee_id
    @ColumnInfo(name = "is_active")
    private int is_active;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShift_id() {
        return shift_id;
    }

    public void setShift_id(String shift_id) {
        this.shift_id = shift_id;
    }

    public String getShift_name() {
        return shift_name;
    }

    public void setShift_name(String shift_name) {
        this.shift_name = shift_name;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }
}
